FROM tiangolo/uwsgi-nginx-flask:flask-python3.5

MAINTAINER Johannes Elferich <elferich@ohsu.edu>

# Add app configuration to Nginx
COPY nginx.conf /etc/nginx/conf.d/
COPY htpasswd /etc/nginx/

RUN pip install ujson
RUN pip install sqlitedict
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
# Copy sample app
COPY ./app /app
CMD ["/start.sh"]
