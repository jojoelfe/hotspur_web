import json
from sqlitedict import SqliteDict
import sys


with open(sys.argv[1],"r") as a:
    data = json.load(a)

mydict = SqliteDict(sys.argv[2])

for a in data:
    mydict[a] = data[a]

mydict.commit()
