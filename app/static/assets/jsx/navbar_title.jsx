var NavbarTitle = React.createClass({

	  getInitialState: function() {
		      var description = '';
		      if (HOTSPUR_ANNOTATION.annotation.description) {
			            description = HOTSPUR_ANNOTATION.annotation.description;
			          }
		      return {
			            edit: false,
			            description: description
			          };
		    },

	  handleClick: function(event) {
		      this.setState({
			            edit: true
			          });
		    },

	  handleChange: function(event) {
		      HOTSPUR_ANNOTATION.annotate(this.props.grid, function(annObj) {
			            annObj.description = event.target.value;
			          });
		      this.setState({
			            edit: false,
			            description: event.target.value
			          });
		    },

	  render: function() {
						const grid = this.props.grid;
						const micrograph = this.props.micrograph;
		      return (
			            <div class="navbar-brand d-flex mx-auto flex-column justify-content-center">
				              <div class="d-flex justify-content-center">
													<a href="javascript:void(0);" id="button_previous"><i class="fa fa-chevron-left" aria-hidden="true"></i> </a> <span id="title_field"></span> <a href="javascript:void(1);"
			                  id="button_next"> <i class="fa fa-chevron-right" aria-hidden="true"></i></a></div>
				              <div class="d-flex flex-row justify-content-center">
				                <span class="navbar-text small p-0" id="grid-anno-field">A good grid</span>
				              </div>
				            </div>
				            );
		    }
});


