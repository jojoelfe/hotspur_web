var path = require('path')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

var webpack = require('webpack')

module.exports = {
  entry: {
	  micrograph : './src/micrograph.js',
	  statistics : './src/statistics.js',
	  montage : './src/montage.js'
  },
  output: {
    path: path.resolve(__dirname, './static/assets/js/'),
    publicPath: '/assets/js/',
    filename: '[name]_build.js'
  },
	node: {
   fs: "empty"
},
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ],
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
          }
          // other vue-loader options go here
        }
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      }
    ]
  },
	plugins: [
    new webpack.ProvidePlugin({
      "$":"jquery",
      "jQuery":"jquery",
      "window.jQuery":"jquery",
      "Tether":"tether"
    }),
	],
  resolve: {
    alias: {
	    "jquery-ui": "jquery-ui/jquery-ui.js", 
      'vue$': 'vue/dist/vue.esm.js'
    },
    extensions: ['*', '.js', '.vue', '.json']
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: false
  },
  devtool: '#eval-source-map'
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new UglifyJsPlugin({
	    uglifyOptions: {
		    ecma: 8
	    }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
