import * as d3 from 'd3';
import HOTSPUR_BASE from './library.js';
import HOTSPUR_ANNOTATION from './annotations.js';
import 'bootstrap';

require('jquery');
require('jquery-ui-bundle');

var LABELS = [{

  "label": "Acquisition Date",
  "callback": function (d) {
    var time = d3.isoParse(d.moviestack.acquisition_time);
    return d3.timeFormat("%B %d, %Y %I:%M %p")(time);
  }
},
{
  "label": "Number of frames",
  "callback": function (d) {
    return d.moviestack.numframes;
  }
},
{
  "label": "Dose rate",
  "callback": function (d) {
    return d3.format(".2f")(d.moviestack.dose_per_pix_frame) + " e/pix/frame";
  }
},
{
  "label": "Defocus",
  "callback": function (d) {
    return "-" + d3.format(".2f")((parseFloat(d.Gctf["Defocus U"]) +
      parseFloat(d.Gctf["Defocus V"])) / 20000) + " µm";
  }
},
{
  "label": "Astigmatism",
  "callback": function (d) {
    return d3.format(".2f")(Math.abs(parseFloat(d.Gctf["Defocus U"]) -
      parseFloat(d.Gctf["Defocus V"])) / 10000) +
      " µm at " + d3.format(".1f")(parseFloat(d.Gctf["Astig angle"])) + "°";
  }
},
{
  "label": "Gctf resolution estimate",
  "callback": function (d) {
    return d3.format(".2f")(d.Gctf["Estimated resolution"]) + " ‫";
  }
},
{
  "label": "Gctf validation score",
  "callback": function (d) {
	  if (!d.Gctf["Validation scores"])  {
		  return undefined;
	  }
    return String(d.Gctf["Validation scores"].reduce(
      function (a, b) {
        return a + parseInt(b);
      },
      0
    )) +
      d.Gctf["Validation scores"].reduce(
        function (a, b) {
          return a + "," + b;
        },
        "("
      ) + ")";
  }
},
];
function clear_montage() {
  d3
    .selectAll("#lmm_micro")
    .attr("src", "");
  d3
    .selectAll("#mmm_micro")
    .attr("src", "");
}
function load_montage(lmm, mmm, micro_item) {
  if (glob_data[lmm] && glob_data[lmm].montage) {
    d3
      .selectAll("#lmm_micro")
      .attr("src", "data/" + glob_data[lmm].montage.preview_filename);
  } else {
    d3
      .selectAll("#lmm_micro")
      .attr("src", "");
  }
  if (mmm && glob_data[mmm].montage) {
    d3
      .selectAll("#mmm_micro")
      .attr("src", "data/" + glob_data[mmm].montage.preview_filename);
  } else {
    d3
      .selectAll("#mmm_micro")
      .attr("src", "");
  }
  setup_mmcanvas(lmm, mmm, micro_item);
}

function setup_mmcanvas(lmm, mmm, micro_item) {
  var canvas = $('#lmm_canvas')[0];
  var navigator = undefined;
  if (lmm.split("_")[0] in glob_data && glob_data[lmm.split("_")[0]].navigator) {
    navigator = lmm.split("_")[0];
  }
  if (lmm.split("_").slice(0, 2).join('') in glob_data && glob_data[lmm.split("_").slice(0, 2).join('')].navigator) {
    navigator = lmm.split("_").slice(0, 2).join('');
  }
  if (navigator == undefined) {
    return;
  }
  var navigator_item = glob_data[navigator].navigator.items.find(function (item) {
    return item.Note && item.Note.split(' ')[3] == lmm + '.mrc';
  });

  //if (navigator_item == undefined) { 
  //}
  if (navigator_item == undefined) {
    return;
  }
  var navigator_items_mmm = glob_data[navigator].navigator.items.filter(function (item) {
    if (item.Note && item.Note.split(' ').length > 3 && item.Note.split(' ')[3].split('_')[0] == lmm.split('_')[0]) {
      return true;
    } else {
      return false;
    }
  });
  if (mmm) {
    var navigator_item_mmm = glob_data[navigator].navigator.items.find(function (item) {
      if (item.Note && item.Note.split(' ')[3] == mmm.substring(0, mmm.length - 3) + '.mrc') {
        if (parseInt(item.Note.split(' ')[1]) == parseInt(mmm.substring(mmm.length - 3, mmm.length))) {
          return true;
        } else {
          return false;
        }
      }
    });
  }
  canvas.width = 1000;
  var map_width = navigator_item.MapWidthHeight.split(' ')[0];
  var width_scale = canvas.width / map_width;
  canvas.height = 1000;
  var map_height = navigator_item.MapWidthHeight.split(' ')[1];
  var height_scale = canvas.height / map_height;
  var image_center = [map_width / 2, map_height / 2];
  var stage_center = navigator_item.RawStageXY.split(' ');
  var scale_coeff = navigator_item.MapScaleMat.split(' ');

  var points = glob_data[navigator].navigator.items.filter(function (item) {
    return navigator_item.MapID == item.DrawnID;
  });
  points = navigator_items_mmm;
  var ctx = canvas.getContext("2d");

  points.forEach(function (point) {
    // Do not use singl PtsX, instead draw box
    if (point.PtsX.split(' ').length < 4) {
      return;
    }
    var coord_x = point.PtsX.split(' ');
    var coord_y = point.PtsY.split(' ');
    ctx.beginPath();
    if (mmm && point.MapID == navigator_item_mmm.MapID) {
      ctx.strokeStyle = "#ff0000";
    } else {
      ctx.strokeStyle = '#ffff00';
    }
    for (var i = 0; i < 5; i++) {
      var stage_coord_begin = [coord_x[i], coord_y[i]];
      var image_coord_begin = calc_image_coordinates(stage_coord_begin, scale_coeff, stage_center, image_center);
      var canvas_coord_begin = [image_coord_begin[0] * width_scale, image_coord_begin[1] * height_scale];
      ctx.lineWidth = 2;
      if (i == 0) {
        ctx.moveTo(canvas_coord_begin[0], canvas_coord_begin[1]);
      } else {
        ctx.lineTo(canvas_coord_begin[0], canvas_coord_begin[1]);
      }
    }
    ctx.closePath();
    ctx.stroke();
  });
  var canvas = $('#mmm_canvas')[0];

  //if (navi gator_item == undefined) { 
  //}
  //
  if (mmm) {
    var svg = d3.selectAll('#mmm_svg');
    svg.selectAll("*").remove();
    svg.attr("preserveAspectRatio", "none")
      .attr("viewBox", "0 0 1000 1000")
      //class to make it responsive
      .classed("svg-content-responsive", true);
    svg.attr('width', 1000);
    var map_width = navigator_item_mmm.MapWidthHeight.split(' ')[0];
    var width_scale = 1000 / map_width;
    svg.attr('height', 1000);
    var map_height = navigator_item_mmm.MapWidthHeight.split(' ')[1];
    var height_scale = 1000 / map_height;
    var image_center = [map_width / 2, map_height / 2];
    var stage_center = navigator_item_mmm.RawStageXY.split(' ');
    var scale_coeff = navigator_item_mmm.MapScaleMat.split(' ');
    var points = glob_data[navigator].navigator.items.filter(function (item) {
      return item.PtsX.split(' ').length < 2 && navigator_item_mmm.StageXYZ.split(' ')[2] == item.StageXYZ.split(' ')[2];
    });
    var point_dataset = points.map(function (point) {
      var coord_x = point.PtsX;
      var coord_y = point.PtsY;
      var stage_coord = [coord_x, coord_y];

      var image_coord = calc_image_coordinates(stage_coord, scale_coeff, stage_center, image_center);
      var svg_x = image_coord[0] * width_scale;
      var svg_y = image_coord[1] * height_scale;
      var res = Object.keys(HOTSPUR_ANNOTATION.annotation).find(function (annot) {
        if (annot.split('/').length > 1 && annot.split('/')[0].replace(/_/g, "") == navigator.replace(/_/g, "")) {

          return annot.split('/')[1].split('_')[1] == point.Title.split(' ')[2];
        }
        return false;
      });
      var micrograph_file = Object.keys(HOTSPUR_BASE.glob_data).find(function (item) {
        if (item.split('/').length > 1 && item.split('/')[0].replace(/_/g, "") == navigator.replace(/_/g, "")) {
          return item.split('/')[1].split('_')[1] == point.Title.split(' ')[2];
        }
        return false;
      });
      var color_code = {
        "good": "#5cb85c",
        "refit": "#f0ad4e",
        "bad": '#d9534f',
        "motion": '#4B0082'
      };
      if (point.MapID == micro_item.MapID) {
        var strokeStyle = '#ff0000';
      } else {
        var strokeStyle = '#222222';
      }

      return {
        x: svg_x,
        y: svg_y,
        color: strokeStyle,
        micrograph: micrograph_file
      };
    });

    var dots = svg.selectAll('g.micrograph')
      .data(point_dataset);
    dots.enter().append('g')
      .attr('class', 'micrograph')
      .attr('transform', function (d) {
        return "translate(" + d.x + "," + d.y + ")";
      })
      .append("path")
      .attr("d", function (d, i, j) {
        return "M-6,-6L6,6M-6,6L6,-6";
      })
      .style("stroke", function (d) {
        return d.color;
      })
      .style("stroke-width", 2)
      .on("mouseover", function () {
        d3.select(this).style("stroke-width", 4);
      })
      .on("mouseout", function () {
        d3.select(this).style("stroke-width", 2);
      })
      .on("click", function (d) {
        if (d.micrograph) {
          var url = "micrograph.html?micrograph=";
          url += d.micrograph;
          window.location = url;
        }
      });
    dots.exit().remove();


  }
}

function calc_image_coordinates(stage_coord, scale_coeff, stage_center, image_center) {
  var X = scale_coeff[0] * (stage_coord[0] - stage_center[0]) + scale_coeff[1] * (stage_coord[1] - stage_center[1]) + image_center[0];
  var Y = scale_coeff[2] * (stage_coord[0] - stage_center[0]) + scale_coeff[3] * (stage_coord[1] - stage_center[1]) + image_center[1];
  return [X, Y];
}
function create_micrograph_info(micrograph) {
  var labels = LABELS;
  d3.selectAll("#micrograph_information").selectAll("div").remove();
  var container = d3.selectAll("#micrograph_information").selectAll("div");
  var label_fields = container.data(labels).enter().append("div")
    .classed("col-xs-4", true);
  label_fields.append("small").text(function (d) {
    return d.label;
  });
  label_fields.append("h4").text(function (d) {
    return d.callback(glob_data[micrograph]);
  });
}




function idogpickerupdate_range(data, sizes, micrograph) {
  var size = sizes[$('#size-slider').slider("value")];
  var range = $('#threshold-slider').slider("values");
  if (!("picks" in HOTSPUR_BASE.glob_data[micrograph])) {
    HOTSPUR_BASE.glob_data[micrograph].picks = {};
  }

  HOTSPUR_BASE.glob_data[micrograph].picks["idogpicker"] = data[size].filter(function (item) {
    var pickfom = parseInt(item[1]);
    if ((pickfom > range[0]) && (pickfom < range[1])) {
      return (true);
    } else {
      return (false);
    }
  }).map(x => ({ "x": x[0][1], "y": x[0][0] }));
  setup_canvas(micrograph);
  HOTSPUR_ANNOTATION.annotate("idogpicker_global", function (object) {
    object.size = size;
    object.threshold = { 'low': range[0], 'high': range[1] };
  });


}




function idogpickerupdate_size(data, sizes, micrograph) {
  var size = sizes[$('#size-slider').slider("value")];
  $("#idogpicker-size").val(size);
  idogpickerupdate_range(data, sizes, micrograph);
}


function setup_idogpicker(micrograph) {
  if ("idogpicker" in glob_data[micrograph])
    d3.json("data/" + glob_data[micrograph]["idogpicker"]["idogpicker_filename"], function (data) {
      var sizes = Object.keys(data).sort((a, b) => a - b);
      $('#size_text').show();
      $('#size-slider').slider(
        {
          min: 0, max: sizes.length - 1
          , step: 1,
          slide: function (event, ui) {
            $(this).slider('value', ui.value);
            idogpickerupdate_size(data, sizes, micrograph);
          }
        });
      var min_range = Math.min(...Object.keys(data).map(x => Math.min(...data[x].map(y => y[1]))));
      var max_range = Math.max(...Object.keys(data).map(x => Math.max(...data[x].map(y => y[1]))));
      var ranges = [min_range, max_range];
      $("#threshold-slider").slider({
        range: true,
        min: min_range,
        max: max_range,
        values: [ranges[0] + 0.1 * (ranges[1] - ranges[0]), ranges[1] - 0.1 * (ranges[1] - ranges[0])],
        slide: function (event, ui) {
          $(this).slider('values', ui.values);
          idogpickerupdate_range(data, sizes, micrograph);
        }

      });

      if (HOTSPUR_ANNOTATION.annotation["idogpicker_global"]) {

        $("#size-slider").slider("value", sizes.findIndex(x => x == HOTSPUR_ANNOTATION.annotation["idogpicker_global"].size));
        if (Array.isArray(HOTSPUR_ANNOTATION.annotation["idogpicker_global"].threshold)) {
          HOTSPUR_ANNOTATION.annotation["idogpicker_global"].threshold = { 'low': HOTSPUR_ANNOTATION.annotation["idogpicker_global"].threshold[0], 'high': HOTSPUR_ANNOTATION.annotation["idogpicker_global"].threshold[1] };
        }

        $("#threshold-slider").slider("values", [HOTSPUR_ANNOTATION.annotation["idogpicker_global"].threshold.low, HOTSPUR_ANNOTATION.annotation["idogpicker_global"].threshold.high]);
      }
      idogpickerupdate_size(data, sizes, micrograph);
      setup_settings(micrograph);
      setup_canvas(micrograph);
    });
}

function setup_canvas(micrograph) {
  if ("picks" in HOTSPUR_BASE.glob_data[micrograph] && Object.keys(HOTSPUR_BASE.glob_data[micrograph]["picks"]).length == 0) {
    d3.json("data_picks?micrograph=" + micrograph, function (data) {
      HOTSPUR_BASE.glob_data[micrograph]["picks"] = data;
      do_setup_canvas(micrograph);
    });
  } else {
    if( ! ( "picks" in HOTSPUR_BASE.glob_data[micrograph] )) {
	    HOTSPUR_BASE.glob_data[micrograph]["picks"] = { 'manual' : {} };
    }
    do_setup_canvas(micrograph);
  }
}

function do_setup_canvas(micrograph) {
  var svg = d3.selectAll('#big_micro_svg');
  svg.selectAll("*").remove();
  if (glob_data[micrograph].MotionCor2 && glob_data[micrograph].MotionCor2.dimensions) {
    var width = glob_data[micrograph].MotionCor2.dimensions[0];
    var height = glob_data[micrograph].MotionCor2.dimensions[1];
    svg.attr('width', glob_data[micrograph].MotionCor2.dimensions[0]);
    svg.attr('height', glob_data[micrograph].MotionCor2.dimensions[1]);
    svg.attr("preserveAspectRatio", "none")
      .attr("viewBox", "0 0 " + width + " " + height)
      //class to make it responsive
      .classed("svg-content-responsive", true);
  }
  if (HOTSPUR_BASE.glob_data[micrograph].picks) {

    function colores_google(n) {
      var colores_g = ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477", "#66aa00", "#b82e2e", "#316395", "#994499", "#22aa99", "#aaaa11", "#6633cc", "#e67300", "#8b0707", "#651067", "#329262", "#5574a6", "#3b3eac"];
      return colores_g[n % colores_g.length];
    }
    //console.log("Annotation before drawing picks:");
    if (HOTSPUR_ANNOTATION.annotation[micrograph] && HOTSPUR_ANNOTATION.annotation[micrograph].picks && HOTSPUR_ANNOTATION.annotation[micrograph].picks.manual) {
      HOTSPUR_BASE.glob_data[micrograph].picks.manual = Object.values(HOTSPUR_ANNOTATION.annotation[micrograph].picks.manual);
    }
    //console.log("Annotation copied over:");
    //console.log(HOTSPUR_BASE.glob_data[micrograph].picks);
    Object.keys(HOTSPUR_BASE.glob_data[micrograph].picks).forEach(function (key, i) {
      if (key == 'idogpicker' && HOTSPUR_ANNOTATION.annotation["idogpicker_global"]) {
        var radius = HOTSPUR_ANNOTATION.annotation["idogpicker_global"].size / 2 + 10;
      } else {
        var radius = 80;
      }

      if ($('#settings-show-' + key.replace(/ /g, '')).prop('checked')) {
        svg.selectAll(".pick_circle." + key.replace(/ /g, '')).data(HOTSPUR_BASE.glob_data[micrograph].picks[key])
          .enter().append("circle")
          .attr("cx", function (d) { return d.x; })
          .attr("cy", function (d) { return d.y; })
          .attr("r", radius)
          .classed("pick_circle", true)
          .classed(key.replace(/ /g, ''), true)
          .classed("active", true)
          .style("stroke", colores_google(i))
          .attr("visibility", function (d) {
            if (d["active"] == 0) { return 'hidden'; } else { return 'visible'; }
          })

          .on("mouseover", function () { d3.select(this).classed("mouseon", true); })
          .on("mouseout", function () { d3.select(this).classed("mouseon", false); })
          .on("contextmenu", function (d) {
            d3.event.preventDefault();

            d["active"] = 0;
            HOTSPUR_ANNOTATION.annotate(micrograph, function (object) {
              if (!object.picks) {

                object.picks = {};
              }
              if (!object.picks[key]) {
                object.picks[key] = {};
              }
              var pickkey = Math.round(d.x) + '_' + Math.round(d.y);
              if (!object.picks[key][pickkey]) {
                object.picks[key][pickkey] = {};
              }
              object.picks[key][pickkey].active = 0;
            });
            d3.select(this).attr("visibility", 'hidden');

          });
      }

    });
  }
  svg.on("click", function () {
    var that = this;
    HOTSPUR_ANNOTATION.annotate(micrograph, function (object) {
      if (!object.picks) {

        object.picks = {};
      }
      if (!object.picks.manual) {
        object.picks.manual = {};
      }
      var x = Math.round(d3.mouse(that)[0]);
      var y = Math.round(d3.mouse(that)[1]);
      var key = x + "_" + y;
      object.picks.manual[key] = { "x": x, "y": y };
    });
    //console.log("Annotation after annotate:");
    //console.log(HOTSPUR_ANNOTATION.annotation[micrograph].picks);
    setup_canvas(micrograph);
    setup_settings(micrograph);
    setup_canvas(micrograph);

  });


  if ($('#settings-show-scalebar').prop('checked')) {
    draw_scalebar(micrograph, svg);
  }
}

function setup_settings(micrograph) {
  $('#settings-show-scalebar').on('change', function () {
    setup_canvas(micrograph);
  });
  $('#settings-picks').empty();
  if (HOTSPUR_BASE.glob_data[micrograph].picks) {
    Object.keys(HOTSPUR_BASE.glob_data[micrograph].picks).forEach(function (key) {
      var pick_item = $("<label></label>").addClass("checkbox").addClass("dropdown-item");
      $('#settings-picks').append(pick_item);
      var input_item = $('<input type="checkbox" id="settings-show-' + key.replace(/ /g, '') + '"></input>');
      input_item.on('change', function () {
        setup_canvas(micrograph);
        HOTSPUR_ANNOTATION.annotate("pick_settings", function (object) {

          Object.keys(HOTSPUR_BASE.glob_data[micrograph].picks).forEach(function (key, i) {
            if ($('#settings-show-' + key.replace(/ /g, '')).prop('checked')) {
              object[key] = true;
            } else {
              object[key] = false;
            }
          });
        });
      });
      input_item.prop('checked', false);
      if (HOTSPUR_ANNOTATION.annotation["pick_settings"] && HOTSPUR_ANNOTATION.annotation["pick_settings"][key]) {
        input_item.prop('checked', true);
      }
      pick_item.append(input_item);
      pick_item.append(key);
    });
  }
}

function draw_scalebar(micrograph, svg) {

  if (HOTSPUR_BASE.glob_data[micrograph].MotionCor2 && HOTSPUR_BASE.glob_data[micrograph].MotionCor2.pixel_size) {
    var width = glob_data[micrograph].MotionCor2.dimensions[0];
    var height = glob_data[micrograph].MotionCor2.dimensions[1];
    var ps = glob_data[micrograph].MotionCor2.pixel_size;
    var scalebar = svg.append('g').attr("id","scalebar")
	                          .attr("transform", "translate(" + String(width-50-1000/ps) +","+String(height-400)+")");
    scalebar.append('rect')
	    .style('fill','#ffffff')
	    .attr('x',0)
	    .attr('y',0)
	    .attr('width',1000/ps)
	    .attr('height',30);
    scalebar.append('text')
	  .attr('x',500/ps)
	  .attr('y',180)
	  .attr('font-family',"Verdana")
	  .attr('font-size',140)
	  .style('text-anchor', 'middle')
	  .attr('fill' , '#ffffff')
	  .text('100 nm');
  }
}

function setup_micrograph_label(micrograph) {
  $('#micrograph_tag').empty();
  if (HOTSPUR_ANNOTATION.annotation[micrograph] && HOTSPUR_ANNOTATION.annotation[micrograph].tag) {
    $('#micrograph_tag').append('<div> </div>');
    $('#micrograph_tag').css("text-align", "center");
    var span = $("#micrograph_tag div");
    span.addClass('badge');
    span.css("width", "100%");
    switch (HOTSPUR_ANNOTATION.annotation[micrograph].tag) {
      case 'good':
        span.addClass('badge-success');
        span.text("Good");
        break;
      case 'motion':
        span.addClass('badge-info');
        span.text("Redo Motioncorrection");
        break;
      case 'bad':
        span.addClass('badge-danger');
        span.text("Bad");
        break;
      case 'refit':
        span.addClass('badge-warning');
        span.text("Refit");
        break;
    }
  }
}


function setup_navbar_title(micrograph) {
  d3.selectAll("#title_field").text(micrograph);
  $('#grid-anno-field').empty();
  var split_micro = micrograph.split('/');
  if (split_micro.length > 1) {
    var grid = split_micro[0];
    $('#grid-anno-input').data('grid', grid);
    if (HOTSPUR_ANNOTATION.annotation[grid] && HOTSPUR_ANNOTATION.annotation[grid].description != '') {
      $('#grid-anno-field').text(HOTSPUR_ANNOTATION.annotation[grid].description);
      $('#grid-anno-input').val(HOTSPUR_ANNOTATION.annotation[grid].description);
    } else {
      $('#grid-anno-field').text("Description").css('font-style', 'italic');
      $('#grid-anno-input').val('');
    }
  }

}

function setup_navbar_events() {
  $('#grid-anno-field').click(function () {
    $('#grid-anno-field').hide();
    $('#grid-anno-form').show();
    $('#grid-anno-input').focus();
  });

  function form_edited(event) {
    if ($(event.target).data('grid')) {
      var grid = $(event.target).data('grid');
      HOTSPUR_ANNOTATION.annotate(grid, function (annObj) {
        annObj['description'] = $(event.target).val();
      });
      if (HOTSPUR_ANNOTATION.annotation[grid] && HOTSPUR_ANNOTATION.annotation[grid].description != '') {
        $('#grid-anno-field').text(HOTSPUR_ANNOTATION.annotation[grid].description);
        $('#grid-anno-input').val(HOTSPUR_ANNOTATION.annotation[grid].description);
      } else {
        $('#grid-anno-field').text("Description").css('font-style', 'italic');
        $('#grid-anno-input').val('');
      }
    }
    $('#grid-anno-field').show();
    $('#grid-anno-form').hide();
  }
  $('#grid-anno-input').focusout(form_edited);
  $('#grid-anno-input').on('keypress', function (event) {
    if (event.keyCode == 13) { // 13 = Enter Key
      $('#grid-anno-form').hide();
    }
  });

}


function load_micrograph(micrograph) {
  window.app.$children[0]._data.micrograph = micrograph;
  curr_index = HOTSPUR_BASE.micrograph_time.findIndex(function (d) {
    return d[0] == micrograph;
  });
  if (HOTSPUR_BASE.glob_data[micrograph].MotionCor2) {
    d3
      .selectAll("#big_micro")
      .attr("src", "data/" + HOTSPUR_BASE.glob_data[micrograph].MotionCor2.preview_filename);
  } else {
    d3
      .selectAll("#big_micro")
      .attr("src", "");
  }
  if (HOTSPUR_BASE.glob_data[micrograph].Gctf) {
    d3
      .selectAll("#gctf_preview")
      .attr("src", "data/" + HOTSPUR_BASE.glob_data[micrograph].Gctf.ctf_preview_image_filename);
  } else {
    d3
      .selectAll("#gctf_preview")
      .attr("src", "");
  }
  setup_navbar_title(micrograph);

  //create_radial_ctf_plot(micrograph);
  //create_motion_chart(micrograph);
  create_micrograph_info(micrograph);
  setup_settings(micrograph);
  setup_canvas(micrograph);
  setup_idogpicker(micrograph);
  setup_micrograph_label(micrograph);

  clear_montage();
  var split_micro = micrograph.split('/');
  if (split_micro.length > 1) {
    var mmm = undefined;
    var micro_item = undefined;
    var grid = undefined;
    if (split_micro[0].split('_').length == 1) {
      grid = split_micro[0];
    } else {
      grid = split_micro[0].split('_')[0];
    }
    if (HOTSPUR_BASE.glob_data[grid] && HOTSPUR_BASE.glob_data[grid].navigator) {
      var navigatora = HOTSPUR_BASE.glob_data[grid].navigator;
      if (split_micro[1].split('_').length == 3) {
        var point = split_micro[1].split('_')[1];
        var micro_item = navigatora.items.find(function (item) {
          if (item.Title.split(' ')[2] == point) {
            return true;
          }
          return false;
        });
        if (micro_item) {
          var mmm_item = navigatora.items.find(function (item) {
            if (item.MapFile) {
              if ((item.StageXYZ) && (item.StageXYZ.split(' ')[2] == micro_item.StageXYZ.split(' ')[2])) {
                return true;
              }
            }
            return false;
          });
          var note_split = mmm_item.Note.split(' ');
          var mmm = note_split[3].slice(0, -4) + ("000" + note_split[1]).slice(-3);
        }
      }
    }





    load_montage(grid + "_lmm", mmm, micro_item);
  }
}


var curr_index;
var glob_data;
var micrograph_time;
var micrograph;


function previous() {
  if (curr_index > 0)
    load_micrograph(micrograph_time[curr_index - 1][0]);
}

function next() {
  if (curr_index < micrograph_time.length - 1)
    load_micrograph(micrograph_time[curr_index + 1][0]);
}

function next_unannot() {
  if (curr_index < micrograph_time.length - 1) {
    while (micrograph_time[curr_index + 1][0] in HOTSPUR_ANNOTATION.annotation) { curr_index += 1; }
    load_micrograph(micrograph_time[curr_index + 1][0]);
  }
}


function set_micrograph_tag(tag) {
  var micrograph = micrograph_time[curr_index][0];
  HOTSPUR_ANNOTATION.annotate(micrograph, function (ann_obj) {
    ann_obj.tag = tag;
  });

  setup_micrograph_label(micrograph);
}

Mousetrap.bind('g', function () {
  set_micrograph_tag('good');
});
Mousetrap.bind('u', function () {
  next_unannot();
});
Mousetrap.bind('m', function () {
  set_micrograph_tag('motion');
});
Mousetrap.bind('b', function () {
  set_micrograph_tag('bad');
});
Mousetrap.bind('r', function () {
  set_micrograph_tag('refit');
});
setup_navbar_events();


HOTSPUR_BASE.setup_counter();
HOTSPUR_BASE.setup_navigation(previous, next);
HOTSPUR_BASE.load_data(function () {
  micrograph = HOTSPUR_BASE.findGetParameter("micrograph");
  glob_data = HOTSPUR_BASE.glob_data;
  micrograph_time = HOTSPUR_BASE.micrograph_time;
  if (micrograph == null) {
    if (HOTSPUR_BASE.glob_data[HOTSPUR_BASE.micrograph_time.slice(-1)[0][0]].MotionCor2) {
      load_micrograph(HOTSPUR_BASE.micrograph_time.slice(-1)[0][0]);
    } else {
      load_micrograph(HOTSPUR_BASE.micrograph_time.slice(-2)[0][0]);
    }
  } else {
    load_micrograph(micrograph);
  }

});

HOTSPUR_ANNOTATION.load_annotation(function () {
  if (micrograph) {
    setup_micrograph_label(micrograph);
  }
});
