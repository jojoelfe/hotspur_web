import Vue from 'vue';
import StatisticsApp from './StatisticsApp.vue';
import './statistics_logic.js';

var vm = new Vue({
  el: '#app',
  render: h => h(StatisticsApp)
});
window.app = vm;
