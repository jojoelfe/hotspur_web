import deepmerge from 'deepmerge'
import * as d3 from 'd3'

const HOTSPUR_ANNOTATION = (function (deepmerge,d3) {
        var my = {};
        // Holds all annotations
        var glob_annotation = {};
        // Holds annotations made by user
        var user_annotation = {};
        // Holds annotations sent to server while waiting for response
        var limbo_annotation = {};
        // Holds annotations loaded from server
        var server_annotation = {};


        // Creates Global annotation list from all three annotation objects
        function merge_annot() {
                var init_annotation = deepmerge(server_annotation,limbo_annotation, {"clone" : true});
		var glob_annotation = deepmerge(init_annotation, user_annotation, {"clone" : true});

                //for (var micrograph in server_annotation) {
                //        glob_annotation[micrograph] = server_annotation[micrograph];
                //}
                //for (var micrograph in limbo_annotation) {
                //        glob_annotation[micrograph] = limbo_annotation[micrograph];
                //}
                //for (var micrograph in user_annotation) {
                //        glob_annotation[micrograph] = user_annotation[micrograph];
                //}
                my.annotation = glob_annotation;
		console.log("Server:" +  Object.keys(server_annotation).length + " Limbo:" + Object.keys(limbo_annotation).length + " User:" + Object.keys(user_annotation).length);
        }

        // Perfoms synchronization cycle: Send user to server, copy to limbo, update server when done
        function sync_annot() {
                if (Object.keys(user_annotation).length > 0 && Object.keys(limbo_annotation).length == 0) {
                        // Set put request
                        $.ajax({
                                type: 'POST',
                                url: 'user_annotation',
                                data: JSON.stringify(user_annotation),
                                contentType: 'application/json;charset=UTF-8',
                                dataType: 'html',
				cache: false,
                                success: function (responseData, textStatus, jqXHR) {
                                        server_annotation = JSON.parse(responseData)['user_annotation'];
					for (var micrograph in limbo_annotation) {
						if (! micrograph in server_annotation) {
							console.log("Alarm! Server did not return annotation");
						}
					}
                                        limbo_annotation = {};
                                        merge_annot();
                                },
                                error: function (responseData, textStatus, errorThrown) {
                                        alert('Error: ' + errorThrown + ". Status: " + textStatus);
                                }
                        });
                        // add user to limbo
                        for (var micrograph in user_annotation) {
                        limbo_annotation[micrograph] = user_annotation[micrograph];
                        }
                        // empty user
                        user_annotation = {};

                        //success callback (delete limbo/replace server annot with new from server)
                        merge_annot();
                }

        }
        setInterval(function(){ 
                sync_annot(); 
                }, 3000);

        // Initially loads annotation from server
        function load_annotation(callback) {
        var noCache = new Date().getTime();
                d3.json("user_annotation" + "?_=" + noCache, function (annotation) {
                        server_annotation = annotation['user_annotation'];
                        merge_annot();
                        
                        callback(my);
                });
        }

        // Performs an annotation
        function annotate(micrograph, annotation_func) {
                if (!user_annotation[micrograph]) {
                        user_annotation[micrograph] = {};
                }
                annotation_func(user_annotation[micrograph]);
                merge_annot();

        }

        my.load_annotation = load_annotation;
        my.annotate = annotate;
        my.annotation = glob_annotation;

        return (my);
}(deepmerge,d3));

window.HOTSPUR_ANNOTATION = HOTSPUR_ANNOTATION;
console.log("annot ran");
export default HOTSPUR_ANNOTATION;
