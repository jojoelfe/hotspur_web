import Vue from 'vue'
import MontageApp from './MontageApp.vue'
import './montage_logic.js'

new Vue({
  el: '#app',
  render: h => h(MontageApp)
})
