import Vue from 'vue';
import MicrographApp from './MicrographApp.vue';
import './micrograph_logic.js';

var vm = new Vue({
  el: '#app',
  render: h => h(MicrographApp)
});
window.app = vm;
