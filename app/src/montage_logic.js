import Countdown from 'countdown'
import * as d3 from 'd3'
import HOTSPUR_BASE from './library.js'
import HOTSPUR_ANNOTATION from './annotations.js'
                
require('jquery');
require('jquery-ui-bundle');
		//This function is called when scripts/helper/util.js is loaded.
		//If util.js calls define(), then this function is not fired until
		//util's dependencies have loaded, and the util argument will hold
		//the module value for "helper/util".
		var glob_data;
		var curr_index;
		var micrograph_time;
		var montage_time;

		function load_montage(montage) {
			var lmm, mmm;
			curr_index = montage_time.findIndex(function (d) {
				return d[0] == montage;
			});
			if (montage.includes("mmm")) {
				lmm = montage.split("_")[0] + "_lmm";
				mmm = montage;
			} else {
				lmm = montage;
				mmm - undefined;
			}
			if (glob_data[lmm].montage) {
				d3
					.selectAll("#lmm_micro")
					.attr("src", "data/" + glob_data[lmm].montage.preview_filename);
			} else {
				d3
					.selectAll("#lmm_micro")
					.attr("src", "");
			}
			if (mmm && glob_data[mmm].montage) {
				d3
					.selectAll("#mmm_micro")
					.attr("src", "data/" + glob_data[mmm].montage.preview_filename);
			} else {
				d3
					.selectAll("#mmm_micro")
					.attr("src", "");
			}
			setup_navbar_title(montage)
			setup_canvas(montage, lmm, mmm);
		}

		function setup_navbar_title(micrograph) {
                        d3.selectAll("#title_field").text(micrograph);
                        $('#grid-anno-field').empty();
                        var split_micro = micrograph.split('_');
                        if (split_micro.length > 1) {
                                var grid = split_micro[0];
                                $('#grid-anno-input').data('grid', grid);
                                if (HOTSPUR_ANNOTATION.annotation[grid] && HOTSPUR_ANNOTATION.annotation[grid].description != '') {
                                        $('#grid-anno-field').text(HOTSPUR_ANNOTATION.annotation[grid].description);
                                        $('#grid-anno-input').val(HOTSPUR_ANNOTATION.annotation[grid].description);
                                } else {
                                        $('#grid-anno-field').text("Description").css('font-style', 'italic');
                                        $('#grid-anno-input').val('');
                                }
                        }

                }

                function setup_navbar_events() {
                        $('#grid-anno-field').click(function () {
                                $('#grid-anno-field').hide();
                                $('#grid-anno-form').show();
                                $('#grid-anno-input').focus();
                        });

                        function form_edited(event) {
                                if ($(event.target).data('grid')) {
                                        var grid = $(event.target).data('grid');
                                        HOTSPUR_ANNOTATION.annotate(grid, function (annObj) {
                                                annObj['description'] = $(event.target).val();
                                        });
                                        if (HOTSPUR_ANNOTATION.annotation[grid] && HOTSPUR_ANNOTATION.annotation[grid].description != '') {
                                                $('#grid-anno-field').text(HOTSPUR_ANNOTATION.annotation[grid].description);
                                                $('#grid-anno-input').val(HOTSPUR_ANNOTATION.annotation[grid].description);
                                        } else {
                                                $('#grid-anno-field').text("Description").css('font-style', 'italic');
                                                $('#grid-anno-input').val('');
                                        }
                                }
                                $('#grid-anno-field').show();
                                $('#grid-anno-form').hide();
                        }
                        $('#grid-anno-input').focusout(form_edited);
                        $('#grid-anno-input').on('keypress', function (event) {
                                if (event.keyCode == 13) { // 13 = Enter Key
                                        $('#grid-anno-form').hide();
                                }
                        });

                }

		function setup_canvas(montage, lmm, mmm) {
			var canvas = $('#lmm_canvas')[0];
			var navigator = undefined;
			if (lmm.split("_")[0] in glob_data && glob_data[lmm.split("_")[0]].navigator) {
				navigator = lmm.split("_")[0];
			}
			if (lmm.split("_").slice(0, 2).join('') in glob_data && glob_data[lmm.split("_").slice(0, 2).join('')].navigator) {
				navigator = lmm.split("_").slice(0, 2).join('');
			}
			if (navigator == undefined) {
				return;
			}
			var navigator_item = glob_data[navigator].navigator.items.find(function (item) {
				return item.Note && item.Note.split(' ')[3] == lmm + '.mrc';
			});

			//if (navigator_item == undefined) { 
			//}
			if (navigator_item == undefined) {
				return;
			}
			var navigator_items_mmm = glob_data[navigator].navigator.items.filter(function (item) {
				if (item.Note && item.Note.split(' ')[3] == mmm.substring(0, mmm.length - 3) + '.mrc') {
					return true;
				} else {
					return false;
				}
			});
			if (mmm) {
				var navigator_item_mmm = glob_data[navigator].navigator.items.find(function (item) {
					if (item.Note && item.Note.split(' ')[3] == mmm.substring(0, mmm.length - 3) + '.mrc') {
						if (parseInt(item.Note.split(' ')[1]) == parseInt(mmm.substring(mmm.length - 3, mmm.length))) {
							return true;
						} else {
							return false;
						}
					}
				});
			}
			canvas.width = 1000;
			var map_width = navigator_item.MapWidthHeight.split(' ')[0];
			var width_scale = canvas.width / map_width;
			canvas.height = 1000;
			var map_height = navigator_item.MapWidthHeight.split(' ')[1];
			var height_scale = canvas.height / map_height;
			var image_center = [map_width / 2, map_height / 2];
			var stage_center = navigator_item.RawStageXY.split(' ');
			var scale_coeff = navigator_item.MapScaleMat.split(' ');

			var points = glob_data[navigator].navigator.items.filter(function (item) {
				return navigator_item.MapID == item.DrawnID;
			});
			points = navigator_items_mmm;
			var ctx = canvas.getContext("2d");

			points.forEach(function (point) {
				// Do not use singl PtsX, instead draw box
				if (point.PtsX.split(' ').length < 4) {
					return;
				}
				var coord_x = point.PtsX.split(' ');
				var coord_y = point.PtsY.split(' ');
				ctx.beginPath();
				if (point.MapID == navigator_item_mmm.MapID) {
					ctx.strokeStyle = "#ff0000";
				} else {
					ctx.strokeStyle = '#ffff00';
				}
				for (var i = 0; i < 5; i++) {
					var stage_coord_begin = [coord_x[i], coord_y[i]];
					var image_coord_begin = calc_image_coordinates(stage_coord_begin, scale_coeff, stage_center, image_center);
					var canvas_coord_begin = [image_coord_begin[0] * width_scale, image_coord_begin[1] * height_scale];
					ctx.lineWidth = 2;
					if (i == 0) {
						ctx.moveTo(canvas_coord_begin[0], canvas_coord_begin[1]);
					} else {
						ctx.lineTo(canvas_coord_begin[0], canvas_coord_begin[1]);
					}
				}
				ctx.closePath();
				ctx.stroke();
			});
			var canvas = $('#mmm_canvas')[0];

			//if (navigator_item == undefined) { 
			//}
			if (mmm) {
				var svg = d3.selectAll('#mmm_svg');
				svg.selectAll("*").remove();
				svg.attr("preserveAspectRatio", "none")
					.attr("viewBox", "0 0 1000 1000")
					//class to make it responsive
					.classed("svg-content-responsive", true);
				svg.attr('width', 1000);
				var map_width = navigator_item_mmm.MapWidthHeight.split(' ')[0];
				var width_scale = 1000 / map_width;
				svg.attr('height', 1000);
				var map_height = navigator_item_mmm.MapWidthHeight.split(' ')[1];
				var height_scale = 1000 / map_height;
				var image_center = [map_width / 2, map_height / 2];
				var stage_center = navigator_item_mmm.RawStageXY.split(' ');
				var scale_coeff = navigator_item_mmm.MapScaleMat.split(' ');
				var points = glob_data[navigator].navigator.items.filter(function (item) {
					return item.PtsX.split(' ').length < 2 && navigator_item_mmm.StageXYZ.split(' ')[2] == item.StageXYZ.split(' ')[2];
				});
				var point_dataset = points.map(function (point) {
					var coord_x = point.PtsX;
					var coord_y = point.PtsY;
					var stage_coord = [coord_x, coord_y];

					var image_coord = calc_image_coordinates(stage_coord, scale_coeff, stage_center, image_center);
					var svg_x = image_coord[0] * width_scale;
					var svg_y = image_coord[1] * height_scale;
					var res = Object.keys(HOTSPUR_ANNOTATION.annotation).find(function (annot) {
						if (annot.split('/').length > 1 && annot.split('/')[0].replace(/_/g, "") == navigator.replace(/_/g, "")) {

							return annot.split('/')[1].split('_')[1] == point.Title.split(' ')[2];
						}
						return false;
					});
					var micrograph_file = Object.keys(HOTSPUR_BASE.glob_data).find(function (item) {
						if (item.split('/').length > 1 && item.split('/')[0].replace(/_/g, "") == navigator.replace(/_/g, "")) {
							return item.split('/')[1].split('_')[1] == point.Title.split(' ')[2];
						}
						return false;
					});
					var color_code = {
						"good": "#5cb85c",
						"refit": "#f0ad4e",
						"bad": '#d9534f'
					}
					if (res && HOTSPUR_ANNOTATION.annotation[res].tag) {
						var strokeStyle = color_code[HOTSPUR_ANNOTATION.annotation[res].tag];
					} else {
						var strokeStyle = '#222222';
					}

					return {
						x: svg_x,
						y: svg_y,
						color: strokeStyle,
						micrograph: micrograph_file
					}
				});

				var dots = svg.selectAll('g.micrograph')
					.data(point_dataset);
				dots.enter().append('g')
					.attr('class', 'micrograph')
					.attr('transform', function (d) {
						return "translate(" + d.x + "," + d.y + ")";
					})
					.append("path")
					.attr("d", function (d, i, j) {
						return "M-6,-6L6,6M-6,6L6,-6";
					})
					.style("stroke", function (d) {
						return d.color;
					})
					.style("stroke-width", 2)
					.on("mouseover", function () {
						d3.select(this).style("stroke-width", 4);
					})
					.on("mouseout", function () {
						d3.select(this).style("stroke-width", 2);
					})
					.on("click", function (d) {
						if (d.micrograph) {
							var url = "micrograph.html?micrograph=";
							url += d.micrograph;
							window.location = url;
						}
					});
				dots.exit().remove();


			}
		}

		function calc_image_coordinates(stage_coord, scale_coeff, stage_center, image_center) {
			var X = scale_coeff[0] * (stage_coord[0] - stage_center[0]) + scale_coeff[1] * (stage_coord[1] - stage_center[1]) + image_center[0];
			var Y = scale_coeff[2] * (stage_coord[0] - stage_center[0]) + scale_coeff[3] * (stage_coord[1] - stage_center[1]) + image_center[1];
			return [X, Y];
		}

		function previous() {
			if (curr_index > 0)
				load_montage(montage_time[curr_index - 1][0]);
		}

		function next() {
			if (curr_index < montage_time.length - 1)
				load_montage(montage_time[curr_index + 1][0]);
		}

		setup_navbar_events();

		HOTSPUR_BASE.setup_counter()
		HOTSPUR_BASE.setup_navigation(previous, next)
		HOTSPUR_BASE.load_data(function () {
			var montage = HOTSPUR_BASE.findGetParameter("montage");
			glob_data = HOTSPUR_BASE.glob_data;
			montage_time = HOTSPUR_BASE.montage_time;

			if (montage == null) {
				load_montage(HOTSPUR_BASE.montage_time.slice(-1)[0][0]);
			} else {
				load_montage(montage);
			}

		});


		HOTSPUR_ANNOTATION.load_annotation(function () {});
