from flask import Flask, send_from_directory, jsonify, request, abort
from flask.json import loads
import ujson
from flask import render_template
import glob
from collections import defaultdict
import pickle, json, csv, os, shutil
import copy
from flask import after_this_request, request
from io import StringIO as IO
import gzip
import functools 
import time
import sys
from sqlitedict import SqliteDict

def gzipped(f):
    @functools.wraps(f)
    def view_func(*args, **kwargs):
        @after_this_request
        def zipper(response):
            accept_encoding = request.headers.get('Accept-Encoding', '')

            if 'gzip' not in accept_encoding.lower():
                return response

            response.direct_passthrough = False

            if (response.status_code < 200 or
                response.status_code >= 300 or
                'Content-Encoding' in response.headers):
                return response
            #gzip_buffer = IO()
            #gzip_file = gzip.GzipFile(mode='wb', 
            #                          fileobj=gzip_buffer)
            #gzip_file.write(response.data)
            #gzip_file.close()


            response.data = gzip.compress(response.data)
            response.headers['Content-Encoding'] = 'gzip'
            response.headers['Vary'] = 'Accept-Encoding'
            response.headers['Content-Length'] = len(response.data)

            return response

        return f(*args, **kwargs)

    return view_func


def data_merge(a, b):
    #print("Local")
    #for key in  a:
    #    print(key)
    #    print(a[key])
    #print("CLient")
    #print(b)

    for key in b:
        if key in a:
            a[key] = data_merge_worker(a[key], b[key])
        else:
            a[key] = b[key]
    #print("Merged")
    #for key in  a:
    #    print(key)
    #    print(a[key])

def data_merge_worker(a, b):
    """merges b into a and return merged result

    NOTE: tuples and arbitrary objects are not handled as it is totally ambiguous what should happen"""
    key = None
    # ## debug output
    # sys.stderr.write("DEBUG: %s to %s\n" %(b,a))
    try:
        if a is None or isinstance(a, str) or isinstance(a, int) or isinstance(a, float):
            # border case for first run or if a is a primitive
            a = b
        elif isinstance(a, list):
            # lists can be only appended
            a = b
        elif isinstance(a, dict):
            # dicts must be merged
            if isinstance(b, dict):
                for key in b:
                    if key in a:
                        a[key] = data_merge_worker(a[key], b[key])
                    else:
                        a[key] = b[key]
            else:
                raise TypeError('Cannot merge non-dict "%s" into dict "%s"' % (b, a))
        else:
            raise TypeError('NOT IMPLEMENTED "%s" into "%s"' % (b, a))
    except TypeError as e:
        raise TypeError('TypeError "%s" in key "%s" when merging "%s" into "%s"' % (e, key, b, a))
    return a

class CustomDefaultdict(defaultdict):
    def __missing__(self, key):
        if self.default_factory:
            dict.__setitem__(self, key, self.default_factory(key))
            return self[key]
        else:
            defaultdict.__missing__(self, key)
#from autoindex.flask_autoindex import AutoIndex
app = Flask(__name__)
debug = False



#AutoIndex(app, browse_root="/hotspur/scratch/")

user_annotation = CustomDefaultdict(
         lambda user: CustomDefaultdict(
             lambda dataset: SqliteDict("{root}/{user}/{dataset}/annotation.db".format(
              root="/hotspur/scratch" if debug else "/scratch",
              user=user,
              dataset=dataset
          ))))

data_cache = defaultdict(lambda: defaultdict(dict))
data_cached = defaultdict(dict)

@app.route("/")
def main():
    if debug:
        datasets = glob.glob("/hotspur/scratch/*/*/data.json")
    else:
        datasets = glob.glob("/scratch/*/*/data.json")
    dataset_dict = defaultdict(lambda : defaultdict(int))
    for dataset in datasets:
        (user,dataset_name) = dataset[9:-10].split('/')[-2:]
        dataset_dict[user][dataset_name] = 1
    return render_template('index.html', dataset_dict=dataset_dict)

@app.route("/<user>/")
def mainuser(user):
    if debug:
        datasets = glob.glob("/hotspur/scratch/{}/*/data.json".format(user))
    else:
        datasets = glob.glob("/scratch/{}/*/data.json".format(user))
    dataset_dict = defaultdict(lambda : defaultdict(int))
    for dataset in datasets:
        (user,dataset_name) = dataset[9:-10].split('/')[-2:]
        dataset_dict[user][dataset_name] = 1
    return render_template('index.html', dataset_dict=dataset_dict)

@app.route("/instructions.html")
def instructions():
    return render_template('instructions.html')

@app.route("/<user>/<dataset>/statistics.html")
def statistics(user, dataset):
    return render_template('statistics.html')

@app.route("/<user>/<dataset>/micrograph.html")
def micrograph(user, dataset):
    return render_template('micrograph.html')

@app.route("/<user>/<dataset>/montage.html")
def montage(user, dataset):
    return render_template('montage.html')

@app.route("/<user>/<dataset>/data",methods=['GET'])
@gzipped
def get_data(user, dataset):
    start = time.time()
    filename = "{root}/{user}/{dataset}/data.json".format(
              root="/hotspur/scratch" if debug else "/scratch",
              user=user,
              dataset=dataset)
    data = {}
    if user in data_cached:
        if dataset in data_cached[user]:
            if time.time() - data_cached[user][dataset] < 60:
                data = copy.deepcopy(data_cache[user][dataset])
                print("used Cache", file=sys.stderr)
    if not data:
        with open(filename, "r") as fp:
            data = ujson.load(fp)
        data_cache[user][dataset] = copy.deepcopy(data)
        data_cached[user][dataset] = time.time()
    read = time.time()
    for micrograph in data:
        if "Gctf" in data[micrograph] and "EPA" in data[micrograph]["Gctf"]:
            data[micrograph]["Gctf"]["EPA"] = {}
        if "picks" in data[micrograph]:
            data[micrograph]["picks"] = {}
    jsonified = time.time()
    print(" S %f  R % f  J %F" % (start, read, jsonified), file=sys.stderr)
    return jsonify(data)

@app.route("/<user>/<dataset>/data_ctf",methods=['GET'])
@gzipped
def get_data_ctf(user, dataset):
    data = {}
    filename = "{root}/{user}/{dataset}/data.json".format(
              root="/hotspur/scratch" if debug else "/scratch",
              user=user,
              dataset=dataset)
    if user in data_cached:
        if dataset in data_cached[user]:
            data = data_cache[user][dataset]
    if not data:
        with open(filename, "r") as fp:
            data = ujson.load(fp)
        data_cache[user][dataset] = copy.deepcopy(data)
        data_cached[user][dataset] = time.time()
    micrograph = str(request.args["micrograph"])

    if micrograph in data and "Gctf" in data[micrograph] and "EPA" in data[micrograph]["Gctf"]:
        return jsonify(data[micrograph]["Gctf"]["EPA"])

    return jsonify({})
    
@app.route("/<user>/<dataset>/data_picks",methods=['GET'])
@gzipped
def get_data_picks(user, dataset):
    data = {}
    filename = "{root}/{user}/{dataset}/data.json".format(
              root="/hotspur/scratch" if debug else "/scratch",
              user=user,
              dataset=dataset)
    if user in data_cached:
        if dataset in data_cached[user]:
            data = data_cache[user][dataset]
    if not data:
        with open(filename, "r") as fp:
            data = ujson.load(fp)
        data_cache[user][dataset] = copy.deepcopy(data)
        data_cached[user][dataset] = time.time()
    if "all" in request.args:
        return jsonify({ micrograph: (data[micrograph]["picks"] if "picks" in data[micrograph] else {}) for micrograph in data })
    micrograph = str(request.args["micrograph"])

    if micrograph in data and "picks" in data[micrograph]:
        return jsonify(data[micrograph]["picks"])

    return jsonify({})

@app.route("/<user>/<dataset>/user_annotation",methods=['GET'])
def get_user_annotation(user, dataset):
    return jsonify({'user_annotation':dict(user_annotation[user][dataset])})

@app.route("/<user>/<dataset>/user_annotation",methods=['POST'])
def add_user_annotation(user, dataset):
    if not request.json:
        abort(404)
    user_annot = request.get_json()
    data_merge(user_annotation[user][dataset],user_annot)
    user_annotation[user][dataset].commit()
    #user_annotation[user][dataset].update(user_annot)

    return jsonify({'user_annotation':dict(user_annotation[user][dataset])})



# Just for Debug

@app.route("/assets/<path:filename>")
def assets(filename):
    return send_from_directory("static/assets/",filename)

@app.route("/<user>/<dataset>/data/<path:filename>")
def data(user,dataset,filename):
    return send_from_directory("/hotspur/scratch/"+user+"/"+dataset,filename)

if __name__ == "__main__":
    debug = True
    app.run(host='0.0.0.0',debug=True, port=8282)
